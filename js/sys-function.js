/*jslint nomen: true*/
/*globals $, alert, console, ActiveXObject, BufferLoader, wx, _hmt*/

var level, velocity, flag, click,
    opt;
opt = {
    assets: {
        path: "image/",
        array: ["f1bg1.jpg",
                "f2p4_1.png",
                "f2p4_2.png",
                "f2p5_1.jpg",
                "f2p5_2.png",
                "f2p5_3.png",
                "f2p5_4.png",
                "../media/op.mp3"
               ]
    }
};

function touchEnd() {
    "use strict";
    flag += 1;
    click += 1;
    $("#f2p3").css("left", flag + "%");
}

function audioPlay() {
    "use strict";
    if ($(".audio").attr("paused") === "0") {
        $("audio")[0].play();
    } else {
        $("audio")[0].pause();
    }
}

function loadImage(url, callback) {
    "use strict";
    var ac, o;
    if (window.XMLHttpRequest) {
        o = new XMLHttpRequest();
    } else if (window.ActiveXObject) {
        o = new ActiveXObject("Microsoft.XMLHTTP");
    }
    o.open("GET", url, true);
    o.send();
    o.onreadystatechange = function () {
        if (o.readyState === 4) {
//            console.log(o.status);
            if (o.status === 200) {
                callback("loaded");
            } else {
                callback("error");
            }
        }
    };
/*
    console.log(o.statusText + "!!!");
    if (o.complete) {
        callback("loaded");
    } else {
        o.onload = function () {
            callback("existed");
        };
        o.onerror = function () {
            callback("error");
        };
    }
*/
}

function loadQue(obj, callback) {
    "use strict";
    var arr, n, url;
    arr = obj.array;
    n = arr.length;
    url = obj.path;
    if (n === 0) {
        callback();
        return false;
    }
    $.each(arr, function (index, ele) {
        loadImage(url + ele, function (s) {
            if (s !== "error") {
                n -= 1;
            } else {
//                n -= 1;
                alert("网速较慢，请刷新重试");
            }
            if (n === 0) {
                callback();
            }
        });
    });
}

function load() {
    "use strict";
    level = "level1";
    velocity = 2;
    flag = 50;
    click = 0;
    $("#game").hide();
    $("#success").hide();
    $("#fail").hide();
    $("body > div").attr("class", "");
    $("body > div > div").hide();
    $("#game").css("background-image", "url('image/f2bg1.jpg')");
    $("#f1p3").attr("class", "flash");
    $("#f2p3").css("left", "50%");
    $(".audio").css("background-position", "left");
    $(".audio").attr("paused", "0");
    $("audio").attr("src", "media/op.mp3");
    $("audio")[0].play();
    document.removeEventListener("touchend", touchEnd, false);

    $("#start").show();
    $("#start").children().each(function (index) {
        if (index < 3) {
            $(this).delay(500 * index).fadeIn();
        }
    });
}

function gameRes(res, n) {
    "use strict";
    var rrr = "",
        lv = "",
        obj,
        obj1;
    switch (level) {
    case "level1":
        lv = "小班";
        break;
    case "level2":
        lv = "中班";
        break;
    case "level3":
        lv = "大班";
        break;
    }
    if (res === "win") {
        rrr = "我一鼓作气发力了" + n.toString() + "次，赢得了" + lv + "的冠军！";
    } else {
        rrr = "吃奶得劲都使出来了，发力了" + n.toString() + "次，差一点就能赢得" + lv + "冠军！谁来挑战？";
    }
    obj = {
        title: '疯狂拔河！',
        desc: rrr,
        link: window.location.href.split("#")[0],
        imgUrl: 'http://comic.qianlong.com/m/game601/img/wximg.jpg',
        type: 'link',
        dataUrl: '',
        success: function () {
            _hmt.push(['_trackEvent', 'game601', 'share', 'weixin']);
        }
    };
    obj1 = {
        title: rrr,
        desc: '疯狂拔河',
        link: window.location.href.split("#")[0],
        imgUrl: 'http://comic.qianlong.com/m/game601/img/wximg.jpg',
        type: 'link',
        dataUrl: '',
        success: function () {
            _hmt.push(['_trackEvent', 'game601', 'share', 'weixin']);
        }
    };
    wx.onMenuShareAppMessage(obj);
    wx.onMenuShareQQ(obj);
    wx.onMenuShareWeibo(obj);
    wx.onMenuShareTimeline(obj1);
}

function gameWin() {
    "use strict";
    $("#game").attr("class", "bounceoutdown").fadeOut("slow");
    $("#success").fadeIn("slow");
    $("#success").children().each(function (index) {
        $(this).delay(500 * index).fadeIn();
    });
    $("audio").attr("src", "media/cheers.mp3");
    $("audio").removeAttr("loop");
    audioPlay();
}

function gameFail() {
    "use strict";
    $("#click").html(click);
    $("#game").attr("class", "bounceoutdown").fadeOut("slow");
    $("#fail").fadeIn("slow");
    $("#fail").children().each(function (index) {
        $(this).delay(500 * index).fadeIn();
    });
    $("audio").attr("src", "media/fail.mp3");
    $("audio").removeAttr("loop");
    audioPlay();
}

function gameStart() {
    "use strict";
    var x, y, z, a = 0;
    $("#start").attr("class", "bounceoutdown").fadeOut("slow");
    switch (level) {
    case "level1":
        velocity = 3;
        $("#f3p1").css("background-image", "url('image/f3p1_1.png')");
        break;
    case "level2":
        velocity = 4;
        $("#f3p1").css("background-image", "url('image/f3p1_2.png')");
        break;
    case "level3":
        velocity = 5;
        $("#f3p1").css("background-image", "url('image/f3p1_3.png')");
        break;
    }
    $("#game").fadeIn("slow");
    $("#f2p1").fadeIn();
    $("#f2p2").delay(200).fadeIn();
    $("#f2p3").delay(700).fadeIn();
    $("#f2p1").children().each(function (index) {
        $(this).delay(1000 * index).fadeIn().fadeOut("slow");
    });
    $("audio").attr("src", "media/ready.mp3");
    audioPlay();
    setTimeout(function () {
        $("#game").css("background-image", "url('image/f2bg2.jpg')");
        $("audio").attr("src", "media/whistle.mp3");
        audioPlay();
        $("audio").removeAttr("loop");
        setTimeout(function () {
            $("audio").attr("src", "media/ing.mp3");
            $("audio").attr("loop", "loop");
            audioPlay();
        }, 500);
        document.addEventListener("touchend", touchEnd, false);
        $("#f2p4").show();
        $("#f2p4_1").show();
        $("#f2p4_2").hide();
        $("#f2p4_3").show();
        y = setInterval(function () {
            $("#f2p4_1").toggle();
            $("#f2p4_2").toggle();
        }, 500);
        $("#f2p5_1").show();
        $("#f2p5_2").show();
        $("#f2p5_3").show();
        $("#f2p5_4").hide();
        z = setInterval(function () {
            a += 10;
            $("#f2p5_1").css("transform", "rotate(" + a + "deg)");
            $("#f2p5_3").toggle();
            $("#f2p5_4").toggle();
        }, 500);
        x = setInterval(function () {
            flag -= velocity;
            $("#f2p3").css("left", flag + "%");
            if (flag <= 0) {
                clearInterval(x);
                clearInterval(y);
                clearInterval(z);
                gameFail();
            }
            if (flag <= 70) {
                $("#f2p4").show();
                $("#f2p5").hide();
            }
            if (flag > 70) {
                $("#f2p4").hide();
                $("#f2p5").show();
            }
            if (flag >= 100) {
                clearInterval(x);
                clearInterval(y);
                clearInterval(z);
                gameWin();
            }
        }, 1000);
    }, 4000);
}

$(function () {
    "use strict";
    loadQue(opt.assets, function () {
        //所有素材加载完成后，loading是显示载入中的DIV
        $("#cover").fadeOut(function () {
            load();
        });
    });
});

window.addEventListener("touchmove", function (e) {
    "use strict";
    e.preventDefault();
});

$("#f1p3").click(function () {
    "use strict";
    if ($(".audio").attr("paused") === "0") {
        $("audio")[0].play();
    }
    $("#f1p3").attr("class", "bounceoutdown").fadeOut("slow");
    $("#f1p4").fadeIn("slow");
});

$("#f1p4 div").click(function () {
    "use strict";
    level = $(this).attr("id");
    gameStart();
});

$(".again").click(function () {
    "use strict";
    load();
});

$(".share").click(function () {
    "use strict";
    $("#sharelay").fadeIn();
});

$("#sharelay").click(function () {
    "use strict";
    $(this).fadeOut();
});

$(".audio").click(function () {
    "use strict";
    if ($(".audio").attr("paused") === "1") {
        $(".audio").attr("paused", "0");
        $(".audio").css("background-position", "left");
    } else {
        $(".audio").attr("paused", "1");
        $(".audio").css("background-position", "right");
    }
    audioPlay();
});